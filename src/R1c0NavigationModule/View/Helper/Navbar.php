<?php

/**
 * Copyright (c) 2014-2015, Rico Döpner
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the names of the copyright holders nor the names of the
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author      Rico Döpner <kontakt@rico-doepner.de>
 * @copyright   2014-2015, Rico Döpner
 * @license     http://www.opensource.org/licenses/bsd-license.php  BSD License
 * @link        http://rico-doepner.de/
 */
namespace R1c0NavigationModule\View\Helper;

use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\Navigation\AbstractContainer;
use Zend\View\Helper\AbstractHelper;
use Zend\View\Model\ViewModel;

class Navbar extends AbstractHelper implements EventManagerAwareInterface
{

    const EVENT_RENDER_PAGE = "render.page";

    protected $container = array();

    protected $eventManager;

    public function __construct(AbstractContainer $container)
    {
        $this->container = $container;
    }

    public function __invoke($options = array())
    {
        $container = $this->container;
        
        $options = array_merge(array(
            'template' => 'widget/navbar',
            'render' => true,
            'position' => '',
            'inverse' => false,
            'fluid' => true,
            'brand' => ''
        ), $options);
        
        $inverse = $options['inverse'];
        $position = $options['position'];
        $brand = $options['brand'];
        $fluid = $options['fluid'];
        $navbar = null;
        
        if ($left = $container->findBy('alignment', 'left')) {
            $navbar = $this->createNavbar($left, 'navbar-left');
        }
        
        if ($right = $container->findBy('alignment', 'right')) {
            $navbar .= $this->createNavbar($right, 'navbar-right');
        }
        
        $viewModel = new ViewModel(array(
            'navbar' => $navbar,
            'position' => $position,
            'inverse' => $inverse,
            'fluid' => $fluid,
            'brand' => $brand
        ));
        
        $viewModel->setTemplate($options['template']);
        
        if ($options['render']) {
            return $this->getView()->render($viewModel);
        } else {
            return $viewModel;
        }
    }

    protected function createNavbar(AbstractContainer $container, $alignment = 'navbar-left')
    {
        $view = $this->getView();
        $plugin = $view->plugin('navigation');
        $plugin->setTranslatorTextDomain('r1c0/navigation');
        $translate = $view->plugin('translate');
        $escaperHtmlAttr = $view->plugin('escapeHtmlAttr');
        $escaperHtml = $view->plugin('escapeHtml');
        $navbar = sprintf('<ul class="nav navbar-nav %s">', $alignment);
        
        foreach ($container as $page) {
            $results = $this->getEventManager()->trigger(self::EVENT_RENDER_PAGE, $this, array(
                'page' => $page
            ), function ($v) {
                return ($v === false);
            });
            
            if (! $results->stopped()) {
                if ($page->isVisible()) {
                    if ($page->hasPages()) {
                        $title = $escaperHtmlAttr($translate($page->getLabel(), 'r1c0/navigation'));
                        $label = $escaperHtml($translate($page->getLabel(), 'r1c0/navigation'));
                        $subHtml = sprintf('<a title="%s" href="#" class="dropdown-toggle" data-toggle="dropdown">%s <b class="caret"></b></a>', $title, $label);
                        $subList = $isActive = null;
                        
                        foreach ($page->getPages() as $subPage) {
                            $results = $this->getEventManager()->trigger(self::EVENT_RENDER_PAGE, $this, array(
                                'page' => $subPage
                            ), function ($v) {
                                return ($v === false);
                            });
                            
                            if (! $results->stopped()) {
                                if ($subPage->isVisible()) {
                                    if ($subPage->isActive()) {
                                        $isActive = true;
                                    }
                                    
                                    if ($subPage->get('type') == "divider") {
                                        $subList .= sprintf('%s', '<li class="divider"></li>');
                                    } else {
                                        $subList .= sprintf('<li%s>%s</li>', $subPage->isActive() ? ' class="active"' : null, $plugin->menu()->htmlify($subPage));
                                    }
                                }
                            }
                        }
                        
                        $subHtml .= sprintf('<ul class="dropdown-menu">%s</ul>', $subList);
                        $navbar .= sprintf('<li class="dropdown%s">%s</li>', $isActive ? ' active' : null, $subHtml);
                    } else {
                        $htmlify = $plugin->menu()->htmlify($page);
                        
                        if ($page->get('icon')) {
                            $matches = array();
                            
                            if (preg_match("#(<[^>]+>)(.*)(</[^>]+>)#U", $htmlify, $matches)) {
                                $icon = sprintf('<span class="glyphicon glyphicon-%s"></span>', $page->get('icon'));
                                $htmlify = sprintf('%s%s %s%s', $matches[1], $icon, $matches[2], $matches[3]);
                            }
                        }
                        
                        $navbar .= sprintf('<li%s>%s</li>', $page->isActive() ? ' class="active"' : null, $htmlify);
                    }
                }
            }
        }
        
        $navbar .= sprintf('%s', '</ul>');
        
        return $navbar;
    }

    public function setEventManager(EventManagerInterface $eventManager)
    {
        $eventManager->setIdentifiers(array(
            'Zend\Stdlib\DispatchableInterface',
            __CLASS__,
            get_class($this),
            substr(get_class($this), 0, strpos(get_class($this), '\\'))
        ));
        
        $this->eventManager = $eventManager;
        
        return $this;
    }

    public function getEventManager()
    {
        if (! $this->eventManager instanceof EventManagerInterface) {
            $this->setEventManager(new EventManager());
        }
        
        return $this->eventManager;
    }
}
