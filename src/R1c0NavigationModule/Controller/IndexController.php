<?php

/**
 * Copyright (c) 2014-2015, Rico Döpner
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the names of the copyright holders nor the names of the
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author      Rico Döpner <kontakt@rico-doepner.de>
 * @copyright   2014-2015, Rico Döpner
 * @license     http://www.opensource.org/licenses/bsd-license.php  BSD License
 * @link        http://rico-doepner.de/
 */
namespace R1c0NavigationModule\Controller;

use R1c0BaseModule\Controller\BaseActionController;
use R1c0NavigationModule\Exception\RuntimeException;
use Zend\Navigation\Service\ConstructedNavigationFactory;
use Zend\Navigation\Navigation;
use Zend\Navigation\Page\Mvc;
use RecursiveIteratorIterator;

class IndexController extends BaseActionController
{

    public function indexAction()
    {
        $response = $this->getResponse();
        $headers = $response->getHeaders();
        $headers->addHeaders(array(
            'Content-Type' => 'text/xml; charset=utf-8'
        ));
        
        $serviceManager = $this->getServiceLocator();
        $mergedConfig = $serviceManager->get('Config');
        $viewHelperManager = $serviceManager->get('ViewHelperManager');
        $navigation = $viewHelperManager->get('navigation');
        $sitemap = $navigation->sitemap();
        $mvcPages = array();
        
        if (! isset($mergedConfig['r1c0']['navigation']['container'])) {
            throw new RuntimeException('Navigation container not found.');
        }
        
        foreach ($mergedConfig['r1c0']['navigation']['container'] as $configContainer) {
            $factory = new ConstructedNavigationFactory($configContainer);
            $container = $factory->createService($serviceManager);
            $iterator = new RecursiveIteratorIterator($container, RecursiveIteratorIterator::SELF_FIRST);
            
            foreach ($iterator as $page) {
                if (($page instanceof Mvc) && ($page->isVisible())) {
                    $mvcPages[] = $page;
                }
            }
        }
        
        $mvcNavigation = new Navigation($mvcPages);
        $response->setContent($sitemap($mvcNavigation)->setFormatOutput(true));
        
        return $response;
    }
}
