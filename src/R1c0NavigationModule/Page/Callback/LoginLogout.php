<?php

/**
 * Copyright (c) 2014-2015, Rico Döpner
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the names of the copyright holders nor the names of the
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author      Rico Döpner <kontakt@rico-doepner.de>
 * @copyright   2014-2015, Rico Döpner
 * @license     http://www.opensource.org/licenses/bsd-license.php  BSD License
 * @link        http://rico-doepner.de/
 */
namespace R1c0NavigationModule\Page\Callback;

use R1c0NavigationModule\Page\Callback\CallableInterface;
use R1c0NavigationModule\Page\Callback\CallbackInterface;
use Zend\Navigation\Page\Mvc as MvcPage;

class LoginLogout implements CallableInterface
{

    public function exec(CallbackInterface $page)
    {
        $sm = $page->getServiceLocator();
        $application = $sm->get('Application');
        $authService = $sm->get('Zend\Authentication\AuthenticationService');
        $options = $page->get('callback_options');
        
        if ($page instanceof MvcPage) {
            $routeMatch = $application->getMvcEvent()->getRouteMatch();
            $router = $application->getMvcEvent()->getRouter();
            
            if ($routeMatch) {
                $page->setRouteMatch($routeMatch);
            }
            
            $page->setRouter($router);
        }
        
        if (! $authService->hasIdentity()) {
            $page->setLabel($options['login']['label']);
            $page->setRoute($options['login']['route']);
            
            if (isset($options['login']['title'])) {
                $page->setTitle($options['login']['title']);
            }
            
            if (isset($options['login']['icon'])) {
                $page->set('icon', $options['login']['icon']);
            }
        } else {
            $page->setLabel($options['logout']['label']);
            $page->setRoute($options['logout']['route']);
            
            if (isset($options['logout']['title'])) {
                $page->setTitle($options['logout']['title']);
            }
            
            if (isset($options['logout']['icon'])) {
                $page->set('icon', $options['logout']['icon']);
            }
        }
    }
}
